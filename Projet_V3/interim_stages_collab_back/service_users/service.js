const express = require("express");
const cors = require("cors");
const Employeur = require("../models/employeur");
const Contact = require("../models/contact");
const Chercheur = require("../models/chercheur");
const Etablissement = require("../models/etablissement");
const connectDB = require("../database/connectDB");
const bcrypt = require("bcrypt");
const { verifyAccessToken } = require("./middlewares/verifyAccessToken");
const axios = require("axios");
const Groupe = require("../models/groupe");
const BloquePartage = require("../models/bloquePartage");
const { mailer } = require("../service_authentication/utils/mailer");

connectDB();

const service = express();
const PORT = 3006;

service.use(cors({ origin: "*" }));
service.use(express.urlencoded({ extended: true }));
service.use(express.json());

// Affichage des requetes recues
service.use((req, res, next) => {
	console.log(`Request received: ${req.method} ${req.url}`);
	next();
});

// Fonction d'enregistrement dans le service registry
const registerService = async (serviceName, serviceVersion, servicePort) => {
	try {
		const response = await axios.put(
			`http://localhost:3001/register/${serviceName}/${serviceVersion}/${servicePort}`
		);
		console.log(response.data); // Log the response from the registry service
	} catch (error) {
		console.error("Error registering service:", error);
	}
};
registerService("users", "v1", PORT);

service.get("/users/profile", verifyAccessToken, async (req, res) => {
	const type = req.decoded.payloadAvecRole.type;
	const userId = req.decoded.payloadAvecRole._id;
	switch (type) {
		case "employeur":
			try {
				const employeur = await Employeur.findById(userId).populate("contacts");

				if (!employeur) {
					return res.status(404).json("Employeur non trouvé");
				}
				res.status(200).json(employeur);
				break;
			} catch (error) {
				return res.status(500).json({ message: "Internal server error" });
			}

		case "chercheur":
			try {
				const chercheur = await Chercheur.findById(userId);
				if (!chercheur) {
					return res.status(404).json("Chercheur non trouvé");
				}
				res.status(200).json(chercheur);
				break;
			} catch (error) {
				return res.status(500).json({ message: "Internal server error" });
			}

		case "etablissement":
			try {
				const etablissement = await Etablissement.findById(userId).populate("contacts");
				if (!etablissement) {
					return res.status(404).json("Etablissement non trouvé");
				}
				res.status(200).json(etablissement);
				break;
			} catch (error) {
				return res.status(500).json({ message: "Internal server error" });
			}			
	}
});

service.put("/users/profile", verifyAccessToken, async (req, res) => {
	const userId = req.decoded.payloadAvecRole._id;
	const type = req.decoded.payloadAvecRole.type;

	switch (type) {
		case "chercheur":
			try {
				const {
					email,
					newPassword,
					oldPassword,
					nom,
					prenom,
					nationalite,
					ville,
					numero,
					cv,
					nom_etablissement,
				} = req.body;
				const chercheur = await Chercheur.findById(userId);
				if (!chercheur) {
					return res.status(404).json({ message: "Chercheur introuvable" });
				}
				if (email) chercheur.email = email;

				if (newPassword) {
					console.log(chercheur, newPassword);
					const isPasswordValid = await bcrypt.compare(
						oldPassword,
						chercheur.password
					);
					if (isPasswordValid) {
						chercheur.password = await bcrypt.hash(newPassword, 10);
					} else {
						return res.status(400).json({ message: "Mot de passe incorrect" });
					}
				}

				if (nom) chercheur.nom = nom;
				if (prenom) chercheur.prenom = prenom;
				if (nationalite) chercheur.nationalite = nationalite;
				if (ville) chercheur.ville = ville;
				if (numero) chercheur.numero = numero;
				if (cv) chercheur.cv = cv;
				if (nom_etablissement) chercheur.nom_etablissement= nom_etablissement;

				await chercheur.save();
				return res.status(200).json({
					user: {
						type: "chercheur",
						email: chercheur.email,
						username: chercheur.nom + " " + chercheur.prenom,
						image: "",
					},
				});
			} catch (error) {
				console.error(error);
				return res.status(500).json({ message: "Internal server error" });
			}
		case "employeur":
			try {
				const {
					email,
					oldPassword,
					newPassword,
					entreprise,
					service,
					sous_service,
					numero_EDA,
					site_web,
					linkedin,
					facebook,
					rue,
					ville,
					contact,
				} = req.body;
				const employeur = await Employeur.findById(userId);
				if (!employeur) {
					return res.status(404).json({ message: "Employeur introuvable" });
				}
				if (email) employeur.email = email;
				if (newPassword) {
					console.log(employeur, newPassword);
					const isPasswordValid = await bcrypt.compare(
						oldPassword,
						employeur.password
					);
					if (isPasswordValid) {
						employeur.password = await bcrypt.hash(newPassword, 10);
					} else {
						return res.status(400).json({ message: "Mot de passe incorrect" });
					}
				}

				if (entreprise) employeur.entreprise = entreprise;
				if (service) employeur.service = service;
				if (sous_service) employeur.sous_service = sous_service;
				if (numero_EDA) employeur.numero_EDA = numero_EDA;
				if (site_web) employeur.site_web = site_web;
				if (facebook) employeur.facebook = facebook;
				if (linkedin) employeur.linkedin = linkedin;
				if (rue) employeur.adresse.rue = rue;
				if (ville) employeur.adresse.ville = ville;

				if (contact) {
					const existingContact = await Contact.findById(contact.id);
					if (contact.nom) {
						existingContact.nom = contact.nom;
					}
					if (contact.email) {
						existingContact.email = contact.email;
					}
					if (contact.numero) {
						existingContact.numero = contact.numero;
					}
					await existingContact.save();
				}

				await employeur.save();
				return res.status(200).json({
					user: {
						type: "employeur",
						email: employeur.email,
						username: employeur.entreprise,
						image: "",
					},
				});
			} catch (error) {
				console.error(error);
				return res.status(500).json({ message: "Internal server error" });
			}
			case "etablissement":

				try {
					const {
					email,
					oldPassword,
					newPassword,
					nom,
					numero,
					type,
					rue,
					ville,
					contact,
				} = req.body;
				const etablissement = await Etablissement.findById(userId);
				if (!etatblissement) {
					return res.status(404).json({ message: "Etablissement introuvable" });
				}
				if (email) etablissement.email = email;
				if (newPassword) {
					console.log(etablissement, newPassword);
					const isPasswordValid = await bcrypt.compare(
						oldPassword,
						etablissement.password
					);
					if (isPasswordValid) {
						etablissement.password = await bcrypt.hash(newPassword, 10);
					} else {
						return res.status(400).json({ message: "Mot de passe incorrect" });
					}
				}

				if (nom) etablissement.nom =nom;
				if (type) etablissement.type = type;
				if (rue) etablissement.adresse.rue = rue;
				if (ville) etablissement.adresse.ville = ville;
				if (numero) etablissement.numero = numero;


				if (contact) {
					const existingContact = await Contact.findById(contact.id);
					if (contact.nom) {
						existingContact.nom = contact.nom;
					}
					if (contact.email) {
						existingContact.email = contact.email;
					}
					if (contact.numero) {
						existingContact.numero = contact.numero;
					}
					await existingContact.save();
				}

				await etablissement.save();
				return res.status(200).json({
					user: {
						type: "etablissement",
						email: etablissement.email,
						username: etablissement.nom,
						image: "",
					},
				});
			} catch (error) {
				console.error(error);
				return res.status(500).json({ message: "Internal server error" });
			}



	}
});





service.post("/users/addMembres", async (req, res) => {
	try {
		const mail = req.body.nouveauMembre;
		const groupeid = req.body.groupId;
		const dateDuJour = new Date().toISOString().split('T')[0];

		//Pas besoin de verifier que le groupe existe?
		const groupe = await Groupe.findById(groupeid);

		//On cherche si l'utilisateur ou chercheur ? existe dans la BDD
		const ChercheurExiste = await Chercheur.findOne({ email: mail });
		if (ChercheurExiste) {

			//On doit vérifier que l'utilisateur n'est pas deja membre du groupe 
			if (groupe.membres.includes(ChercheurExiste._id)) {

				res.status(201).json({ message: "l'utilisateur fait déja partie du groupe" });
			}
			else {

				groupe.membres.push(ChercheurExiste);

				await groupe.save();

				const NomsMembres = groupe.membres.map(Chercheur => Chercheur.nom);

				res.status(200).json({
					message: "l'utilisateur a bien été ajouté a groupe",
					membres: NomsMembres
				});
			}

			//Envoie de la notif ,On vérifie que l'utilisateur qui rejoint le groupe n'a pas bloqué les notifs du createur du groupe , cependant comment savoir s'il a bloquer les notifs de partage pour tout le monde?
			const bloquePartage = await BloquePartage.findOne({emetteur : ChercheurExiste, concerne :groupe.createur});

			//Faudrait partager l'offre en notification
			if(!bloquePartage){
				const notification = new Notification({
					type: "invitation",
					contenu: `Vous avez été ajouté au groupe ${groupe.nom} par : ${req.body.nom}`,
					date_creation: dateDuJour,
					statut: "non lu",
					type_recepteur: "chercheur",
					recepteur: ChercheurExiste._id,
				});

			}
		}
		//L'utilisateur n'existe pas on lui envoie une notification push avec fireBase
		else{
			// Envoyer l'e-mail de confirmation
			await mailer.sendMail({
                from: `${process.env.EMAIL}`,
                to: mail,
                subject: "Invitation à un groupe",
                text: `Vous avez été ajouté au groupe ${groupe.nom} par : ${req.body.nom}`,
            });
			console.log("Email envoyé");

		}
	} catch (error) {

		res.status(201).json({ message: 'Erreur ajout membre' });
	}

});

//Un post pour creer des grp 
service.post("/users/groupes", async (req, res) => {
	try {

		//Ici on doit recuperer les infos de l'utilisateur on modifie avec findbyid
		const Createur = await Chercheur.findById(req.body.createur);
		const groupe = new Groupe({
			nom: req.body.nom,
			createur: Createur,
			membres: [Createur],
			description: req.body.description
		});

		const groupeCree = await groupe.save();
		res.status(200).json({ message: 'groupe crée avec succées' });

	}
	catch (error) {
		res.status(500).json({ message: 'erreur lors de la création du groupe' });
	}
});




service.post("/users/getGroupes", async (req, res) => {
	try {
		const ChercheurId = req.body.id;
		const groupes = await Groupe.find({ membres: ChercheurId });

		const groupesAvecNoms = [];
		for (let groupe of groupes) {
			// Récupérer les noms des membres du groupe
			const membres = await Chercheur.find({ _id: { $in: groupe.membres } });
			const groupeAvecNoms = {
				...groupe.toObject(), // Copier tous les attributs du groupe
				nomsMembres: membres.map(membre => membre.nom)
			};
			groupesAvecNoms.push(groupeAvecNoms);
		}
		res.status(200).json({ message: 'Tous les groupes ont été trouvés', groupes: groupesAvecNoms });
	} catch (error) {
		console.error("Erreur dans la recherche des groupes dont le Chercheur est membre :", error);
		res.status(500).json({ message: 'Erreur dans la recherche des groupes dont le Chercheur est membre' });
	}
});
service.listen(PORT, () => {
	console.log(`service running on port ${PORT}`);
});
