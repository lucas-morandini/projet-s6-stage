const nodemailer = require("nodemailer");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const mailer = nodemailer.createTransport({
	host: "smtp.gmail.com",
	port: 587,
	secure: false,
	auth: {
		user: `${process.env.EMAIL}`,
		pass: `${process.env.EMAIL_PASSWORD}`,
	},
});

module.exports = { mailer };
