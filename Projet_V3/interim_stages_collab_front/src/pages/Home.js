import React from "react";
import { useState, useEffect } from "react";
import {
	Header,
	WelcomeDiv,
	BarreEmployeurs,
	Cadres,
	BarreRecherche,
	Spinner,
} from "../components";
import { axiosInstance } from "../util/axios";

export function Home() {
	let data = [
		{
			employeur: "Entreprise X",
			"Date de publication": "15 avril 2024",
			titre: "Développeur Web",
			Localisation: "Paris, France",
			Salaire: "À négocier",
			Duree: "3 mois",
			Description: "Nous recherchons un stagiaire développeur web pour rejoindre notre équipe dynamique. Votre mission consistera à participer au développement et à la maintenance de nos applications web. Vous travaillerez sur des projets variés et aurez l'opportunité d'apprendre et de développer vos compétences en programmation."
		},
		{
			employeur: "Entreprise Y",
			"Date de publication": "20 avril 2024",
			titre: "Analyste en données",
			Localisation: "Montréal, Canada",
			Salaire: "15$/heure",
			Duree: "4 mois",
			Description: "Rejoignez notre équipe en tant que stagiaire analyste en données. Vous serez chargé de collecter, analyser et interpréter les données pour fournir des informations précieuses à l'entreprise. Vous travaillerez sur des projets stimulants dans un environnement collaboratif et innovant."
		},
		{
			employeur: "Entreprise Z",
			"Date de publication": "25 avril 2024",
			titre: "Ingénieur en sécurité informatique",
			Localisation: "Bruxelles, Belgique",
			Salaire: "2000€/mois",
			Duree: "6 mois",
			Description: "Nous sommes à la recherche d'un stagiaire passionné par la sécurité informatique pour rejoindre notre équipe. Vous participerez à l'évaluation des risques, à la mise en place de mesures de sécurité et à la gestion des incidents. Cette opportunité vous permettra d'acquérir une expérience précieuse dans un domaine en constante évolution."
		}
	];

	async function getOffres() {
		try {
			setShowLoading(true);
			const response = await axiosInstance.get("/offres");

			console.log(response);

			if (response.request.status === 200) {
				setOffres(response.data);
				setShowLoading(false);
			}
		} catch (e) {
			console.log(e);
			setShowLoading(false);
		}
	}

	useEffect(() => {
		getOffres();
	}, []);

	const [offres, setOffres] = useState(data);

	const getResults = (search) => {
		return [];
	};

	const [searchOn, setSearchOn] = useState(false);
	const [showLoading, setShowLoading] = useState(false);

	const [search, setSearch] = useState("");

	const handleSearch = (search) => {
		setShowLoading(true);
		setTimeout(() => {
			setOffres(getResults(search));
			setSearch(search);
			setSearchOn(true);
			setShowLoading(false);
		}, 1000);
	};

	const handleAdvancedSearch = () => {
		setShowLoading(true);
		setTimeout(() => {
			setOffres(getResults("Votre recherche"));
			setSearch("Votre recherche avancée");
			setSearchOn(true);
			setShowLoading(false);
		}, 1000);
	};

	return (
		<div className='min-h-screen bg-bleu'>
			<Header />
			<BarreRecherche
				onSuggestionClick={handleSearch}
				onAdvancedSearchClick={handleAdvancedSearch}
			></BarreRecherche>

			{!searchOn && (
				<>
					<WelcomeDiv></WelcomeDiv>
					<Cadres search={search} data={offres}></Cadres>
				</>
			)}

			{searchOn && <Cadres search={search} data={offres}></Cadres>}
			{showLoading && <Spinner />}
		</div>
	);
}
