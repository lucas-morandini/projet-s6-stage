import React from "react";
import { Header, InscriptionEtablissement } from "../../components";

export function RegisterEtablissement() {
	return (
		<div className='min-h-screen bg-bleu p-5 items-center justify-center flex w-full'>
			<InscriptionEtablissement></InscriptionEtablissement>
		</div>
	);
}