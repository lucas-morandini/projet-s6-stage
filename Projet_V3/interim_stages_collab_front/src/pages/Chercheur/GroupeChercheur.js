import React from "react";
import { HeaderChercheur, NavBarChercheur } from "../../components";
import { GestionGroupe } from "../../components/GestionGroupe";

export function GroupeChercheur() {
    return (
        <div className="dropdown-container" >
            <HeaderChercheur></HeaderChercheur>
            <GestionGroupe></GestionGroupe>
        </div>
    );
}
