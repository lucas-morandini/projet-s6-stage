import React, { useState, useEffect } from "react";
import {
    HeaderChercheur,
    NavBarChercheur,
    Spinner,
    Apply,
    Avertissement,
    CadreApply,
} from "../../components";
import { useParams } from "react-router-dom";
import { axiosInstance } from "../../util/axios";
import {type} from "@testing-library/user-event/dist/type";

export function PostulerChercheur() {
    const [data, setData] = useState({});
    const [loading, setLoading] = useState(false);
    const [candidatures, setCandidatures] = useState([]);
    const [showAvertissement, setShowAvertissement] = useState(false);
    const [offre, setOffre] = useState({});

    let { id } = useParams();

    async function getOffre() {
        try {
            const response = await axiosInstance.get(`/offres/${id}`);
            if (response.status === 200) {
                setOffre(response.data);
                console.log("getOffre:offre"+ offre);
            } else {
                console.log(response);
            }
        } catch (e) {
            console.log(e);
        }
    }

    async function getCandidatures() {
        try {
            setLoading(true);
            let accessToken = localStorage.getItem("accessToken");
            const response = await axiosInstance.get("/candidatures/chercheur", {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            });

            console.log(response);

            if (response.status === 200) {
                const dataEnAttente = response.data.filter(
                    (item) => item.status === "En attente"
                );
                setCandidatures(dataEnAttente);
                setLoading(false);
            }
        } catch (e) {
            console.log(e);
            setLoading(false);
        }
    }

    async function addCandidature(data) {
        try {
            setLoading(true);
            let accessToken = localStorage.getItem("accessToken");
            const response = await axiosInstance.post(
                "/candidatures/chercheur/add",
                data,
                {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                }
            );

            console.log(response);

            if (response.status === 201) {
                setLoading(false);
            }
        } catch (e) {
            console.log(e);
            setLoading(false);
        }
    }

    const redirect = () => {
        window.location.href = "/offres/" + id;
    };

    useEffect(() => {
        getOffre();
        getCandidatures();

    }, []);

    useEffect(() => {
        setShowAvertissement(candidatures.length > 0);
    }, [candidatures]);

    return (
        <div className="min-h-screen bg-bleu pb-10">
            <HeaderChercheur></HeaderChercheur>
            <div className={"flex "}>
                <div className="m-6 bg-white rounded-lg p-4 w-1/2">
                    <CadreApply data={offre} />
                </div>
                <div className="m-6 bg-white rounded-lg p-4">
                    <Apply data={candidatures} onConfirm={addCandidature} />
                </div>
            </div>
            {loading && <Spinner />}
            {showAvertissement && (
                <Avertissement
                    Titre={"Avertissement"}
                    Texte={`Vous postulez pour ${
                        candidatures.length
                    } offre${candidatures.length > 1 ? "s" : ""} d'emploi déjà. Voulez-vous continuer ?`}
                    onConfirm={() => setShowAvertissement(false)}
                    onDismiss={() => redirect()}
                />
            )}
        </div>
    );
}
