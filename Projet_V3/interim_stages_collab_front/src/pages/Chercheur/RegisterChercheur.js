import React from "react";
import { Header, InscriptionChercheur } from "../../components";

export function RegisterChercheur() {
	return (
		<div className='min-h-screen bg-bleu p-5 items-center justify-center flex w-full'>
			<InscriptionChercheur></InscriptionChercheur>
		</div>
	);
}
