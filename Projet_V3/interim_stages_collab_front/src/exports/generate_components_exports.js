const fs = require('fs');
const path = require('path');

const componentsDir = '../components'; // Remplacez par le chemin de votre répertoire de composants

fs.readdir(componentsDir, (err, files) => {
  if (err) {
    console.error('Erreur de lecture du répertoire des composants :', err);
    return;
  }

  const exportStatements = files
    .filter(file => file !== 'index.js' && file.endsWith('.js'))
    .map(file => `export * from "./${file.slice(0, -3)}";`)
    .join('\n');

  fs.writeFile(path.join(componentsDir, 'index.js'), exportStatements, err => {
    if (err) {
      console.error('Erreur d\'écriture du fichier index.js :', err);
      return;
    }
    console.log('Fichier index.js généré avec succès !');
  });
});
