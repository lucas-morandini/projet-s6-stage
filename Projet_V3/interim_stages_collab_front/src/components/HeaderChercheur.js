import React, { useState } from "react";
import Notifications from "./Notifications";
import Compte from "./Compte";
import {NavBarAgence} from "./NavBarAgence";
import {NavBarChercheur} from "./NavBarChercheur";

export function HeaderChercheur({selected}) {



	const redirect = () => {
		window.location.href = "/chercheur";

	}
	return (
		<div className='py-4 bg-bleuF'>
			<div className='flex mx-10 items-center justify-between'>
				<h1 className='text-2xl font-bold text-violet hover:cursor-pointer' onClick={redirect}>Stages - Etudiant</h1>
				<nav className='flex items-center justify-center space-x-8'>
					<NavBarChercheur selected={selected}/>
					<ul className='flex space-x-6 items-center'>
						<li className='cursor-pointer relative'>
							<Notifications/>
						</li>
						<li className='cursor-pointer'>
							<Compte/>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	);
}
