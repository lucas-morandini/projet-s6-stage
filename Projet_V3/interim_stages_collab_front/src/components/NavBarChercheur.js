import React, { useState } from "react";
import { NavLink } from "react-router-dom";

export function NavBarChercheur({ selected }) {
	const [selectedItem, setSelectedItem] = useState(selected);

	return (
		<div className='bg-bleuF text-bleu'>
			<div className='flex items-center justify-center'>
				<nav>
					<ul className='flex space-x-8 items-center'>
						<li
							className={`px-2 py-1 cursor-pointer text-base font-bold ${
								selectedItem === 0 ? "bg-bleuF text-bleu" : "text-bleu"
							}`}
						>
							<NavLink to={"/chercheur/candidatures"}>Candidatures</NavLink>
						</li>
						<li
							className={`px-2 py-1 cursor-pointer text-base font-bold ${
								selectedItem === 1 ? "bg-bleuF text-bleu" : "text-bleu"
							}`}
						>
							<NavLink to={"/chercheur/stages"}>Stages</NavLink>
						</li>
						<li
							className={`px-2 py-1 cursor-pointer text-base font-bold ${
								selectedItem === 2 ? "bg-bleuF text-bleu" : "text-bleu"
							}`}
						>
							<NavLink to={"/chercheur/conventions"}>Conventions</NavLink>
						</li>
						<li
							className={`px-2 py-1 cursor-pointer text-base font-bold ${
								selectedItem === 3 ? "bg-bleuF text-bleu" : "text-bleu"
							}`}
						>
							<NavLink to={"/chercheur/groupe"}>Groupe</NavLink>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	);
}
