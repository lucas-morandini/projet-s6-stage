import React from "react";
import { FaEllipsisV } from "react-icons/fa";
import { ButtonRond } from "./ButtonRond";
import um from "../assets/logo_um.png";
import { fDate } from "../util/formatTime";

export function CadreGEmployeur({ Offre }) {
	return (
		<div className='w-full bg-violet rounded-lg p-4'>
			<div className='flex'>
				<div>
					<p className='text-bleuF font-bold text-xl'>{Offre.titre}</p>
					<div className='flex'>
						<p className='text-bleuF'>{fDate(Offre.date)}</p>
						<p className='text-bleuF ml-4'>Montpellier</p>
						<p className='text-bleuF ml-4'>
							{Offre.candidatures ? Offre.candidatures.length : ""} candidats
						</p>
					</div>
				</div>
				<div className='ml-auto my-auto'>
					<FaEllipsisV />
				</div>
			</div>
			<div className='grid grid-cols-2 gap-x-4'>
				<div className='mt-2'>
					<img className='bg-white w-full h-48 rounded-lg' src={um}></img>
				</div>
				<div className='px-10'>
					<p className='text-bleuF font-bold text-lg'>A propos de l'offre</p>
					<div className='mt-2'>
						<p className='text-bleuF font-bold'>Description</p>
						<p className='text-sm text-bleuF'>{Offre.description} </p>
					</div>
					<div className='mt-2'>
						<p className='text-bleuF font-bold'>Conditions</p>
						<ul className='list-disc text-bleuF'>
							<li>
								<p className='text-sm text-bleuF'>Connaissances en NodeJs et React</p>
							</li>
							<li>
								<p className='text-sm text-bleuF'>Usages Databases</p>
							</li>
							<li>
								<p className='text-sm text-bleuF'>Excellent en communication</p>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div></div>
			<div className='flex justify-end m-4 space-x-2'>
				<ButtonRond
					couleur={"bleuF"}
					couleurTexte={"violet"}
					contenu={"Voir candidatures"}
					width={"fit"}
					height={"fit"}
				></ButtonRond>
				<ButtonRond
					couleur={"rouge"}
					couleurTexte={"violet"}
					contenu={"Archiver"}
					width={"fit"}
					height={"fit"}
				></ButtonRond>
			</div>
		</div>
	);
}
