import React, { useState, useEffect } from "react";
import { useParams } from 'react-router-dom';
import { FaEllipsisV, FaDollarSign } from "react-icons/fa";
import { MdLocationOn } from "react-icons/md";
import { TiTime } from "react-icons/ti";
import { ButtonRond } from "./ButtonRond";
import { Modal } from "./Modal";
import { axiosInstance } from "../util/axios";

export function CadreApply() {


    // let fakeData = {
    // 	employeur: "KPMG",
    // 	"Date de publication": "12 Décembre, 20:20",
    // 	titre: "Developpeur Fullstack",
    // 	Localisation: "Montpellier",
    // 	Salaire: "19$/heure",
    // 	Duree: "2 semaines",
    // 	description:
    // 		"Nous recherchons un stagiaire développeur web pour rejoindre notre équipe dynamique. Votre mission consistera à participer au développement et à la maintenance de nos applications web. Vous travaillerez sur des projets variés et aurez l'opportunité d'apprendre et de développer vos compétences en programmation." };

    const { id } = useParams();
    const [offre, setOffre] = useState({});

    useEffect(() => {
        getOffre();
    }, []);

    async function getOffre() {
        try {
            console.log("getOffre:id:"+id);
            const response = await axiosInstance.get(`/offres/${id}`);
            if (response.status === 200) {
                setOffre(response.data);
                console.log("getOffre:offre:"+offre);
            } else {
                console.log(response);
            }
        } catch (e) {
            console.log(e);
        }
    }


    return (
        <div className='w-full bg-violet rounded-lg border-2 border-bleuF'>
            <div className='flex px-10 py-2'>
                <div>
                    <p className='text-bleuF font-bold text-xl'>{offre.titre}</p>
                    <ul className='list-disc text-bleuF'>
                        <li className='text-bleuF'>
                            <p className='text-bleuF font-bold'>Employeur</p>
                            {offre.employeur ? offre.employeur.entreprise : ""}
                        </li>
                        <div className='flex '>
                            <li className='text-bleuF mr-4'>
                                <p className='text-bleuF font-bold'>Date de début</p>
                                {offre.debut}
                            </li>
                            <li className='text-bleuF ml-4'>
                                <p className='text-bleuF font-bold'>Date de fin</p>
                                {offre.fin}
                            </li>
                        </div>

                        <li className='text-bleuF'>
                            <p className='text-bleuF font-bold'>Salaire</p>
                            {offre.remuneration}
                        </li>

                        {/* Ajouter la Localisation dans la base de données et tout ce qui suit  */}
                        <li className='text-bleuF'>
                            <p className='text-bleuF font-bold'>Localisation</p>
                            {offre.localisation}
                        </li>
                    </ul>
                </div>
            </div>

            <div className='px-10 py-2'>
                <p className='text-bleuF font-bold text-lg'>A propos de l'offre</p>
                <div className='mt-2'>
                    <p className='text-bleuF font-bold'>Description</p>
                    <p className='text-sm text-bleuF'>{offre.description} </p>
                </div>

                {/* Rajout d'une liste de conditions dans la base de donnée offre */}
                <div className='mt-2'>
                    <p className='text-bleuF font-bold'>Conditions</p>
                    <ul className='list-disc text-bleuF'>
                        <li>
                            <p className='text-sm text-bleuF'>Connaissances en NodeJs et React</p>
                        </li>
                        <li>
                            <p className='text-sm text-bleuF'>Savoir utiliser des base de données</p>
                        </li>
                        <li>
                            <p className='text-sm text-bleuF'>Excellent en communication</p>
                        </li>
                    </ul>
                </div>
                <div className='mt-2'>
                </div>
            </div>

            <div></div>
        </div>
    );
}
