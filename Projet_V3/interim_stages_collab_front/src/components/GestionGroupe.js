import React, { useState, useEffect } from "react";
import { axiosInstance } from "../util/axios";
import '../pages/Chercheur/DropdownMenu.css'; // Fichier CSS pour les styles
export function GestionGroupe() {
    // Récupérer l'objet et le convertir en objet JavaScript
    const infoUser = JSON.parse(localStorage.getItem("user"));
    const [groupes, setGroupes] = useState([]);
    const [groupsCreatedByMe, setGroupsCreatedByMe] = useState([]);
    const [groupsImMember, setGroupsImMember] = useState([]);


    function isValidInput(input) {
        // Expression régulière pour valider un email
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        // Expression régulière pour valider un numéro de téléphone
        const phoneRegex = /^\d{10}$/;

        // Vérifier si l'entrée correspond à un email ou un numéro de téléphone
        return emailRegex.test(input) || phoneRegex.test(input);
    }

    async function fetchData() {
        try {
            console.log("id", infoUser.id)
            // Envoyer une requête POST pour récupérer les groupes du chercheur
            const response = await axiosInstance.post('/users/getGroupes', {
                id: infoUser.id
            });

            setGroupes(response.data.groupes);
        } catch (error) {
            // Gérer les erreurs
            console.error('Erreur lors de la récupération des groupes:', error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    useEffect(() => {
        // Separate groups based on creator and member
        const groupsCreatedByCurrentUser = groupes.filter(group => group.createur === infoUser.id);
        const groupsWhereImMember = groupes.filter(group => group.membres.includes(infoUser.id) && group.createur !== infoUser.id);

        setGroupsCreatedByMe(groupsCreatedByCurrentUser);
        setGroupsImMember(groupsWhereImMember);
    }, [groupes, infoUser.id]);

    const [expandedItemsCreatedByMe, setExpandedItemsCreatedByMe] = useState(new Array(groupsCreatedByMe.length).fill(false));
    const [expandedItemsImMember, setExpandedItemsImMember] = useState(new Array(groupsImMember.length).fill(false));

    const toggleDropdownCreatedByMe = (index) => {
        setExpandedItemsCreatedByMe((prevExpandedItems) => {
            const newExpandedItems = [...prevExpandedItems];
            newExpandedItems[index] = !newExpandedItems[index];
            return newExpandedItems;
        });
    };

    const toggleDropdownImMember = (index) => {
        setExpandedItemsImMember((prevExpandedItems) => {
            const newExpandedItems = [...prevExpandedItems];
            newExpandedItems[index] = !newExpandedItems[index];
            return newExpandedItems;
        });
    };
    const ajouterMembre = async (groupId) => {
        let nouveauMembre = document.getElementById(groupId + 'a').value;
        console.log("Ajouter un nouveau membre au groupe avec l'ID :", groupId, " nom :", nouveauMembre);
        if (isValidInput(nouveauMembre)) {
            try {
                const response = await axiosInstance.post('/users/addMembres', {
                    groupId: groupId,
                    nouveauMembre: nouveauMembre,
                    nom : infoUser.username,
                });

                console.log("Réponse du serveur :", response.data);
                fetchData();
            } catch (error) {
                console.error("Erreur lors de l'ajout du membre :", error);
            }
        }
        else {
            alert("Information du nouveau membre doit etre un email ou un numéro de téléphone.");
        }
    };

    const ajouterGroupe = async () => {
        let nomgroupe = document.getElementById('nomInput').value;
        let descriptiongroupe = document.getElementById('descriptionGroupe').value;
        console.log("Ajouter un nouveau groupe avec nom :", nomgroupe, ' createur :', infoUser.id, 'description :', descriptiongroupe);
        if (nomgroupe.trim() === '') {
            alert("Nom du groupe manquant.");
            return;
        }
        try {
            const response = await axiosInstance.post('/users/groupes', {
                nom: nomgroupe,
                createur: infoUser.id,
                description: descriptiongroupe
            });
            // Appel de fetchData avec les arguments appropriés
            console.log("Réponse du serveur :", response.data);
            fetchData();
        } catch (error) {
            console.error("Erreur lors de l'ajout du groupe :", error);
        }
    };

    return (
        <div>
            <div className='grid grid-cols-4 gap-8 mx-4 mb-10'>
                <div className='flex flex-col'>

                    <label className='text-noir text-xs font-bold'>Nom du groupe :</label>
                    <input
                        id="nomInput"
                        className='bg-violet border border-gray-400 rounded-md p-1 focus:outline-none focus:border-blue-500'
                        type='text'

                    />
                    <label className='text-noir text-xs font-bold'>Description du groupe :</label>
                    <input
                        id="descriptionGroupe"
                        className='bg-violet border border-gray-400 rounded-md p-1 focus:outline-none focus:border-blue-500'
                        type='text'
                    />
                </div>
            </div>
            <button onClick={ajouterGroupe} className={`rounded 10  bg-rouge text-noir text-sm font-bold px-4 py-2`} >Ajouter un groupe</button>

            <h3 className='text-xl text-noir font-bold'>Mes groupes créateur :</h3>
            {groupsCreatedByMe.map((groupe, index) => (
                <div className="dropdown-container" key={index}>
                    <p onClick={() => toggleDropdownCreatedByMe(index)} className="toggle-text" style={{ fontWeight: 'bold' }} >
                        {groupe.nom}

                    </p>
                    <div className={`dropdown-content ${expandedItemsCreatedByMe[index] ? 'expanded' : ''}`}>
                        <p>Membres :</p>
                        <ul>
                            {groupe.nomsMembres.map((membre, i) => (
                                <li key={i}>{membre}</li>
                            ))}

                        </ul>
                        Email ou Numéro de téléphone :
                        <input id={`${groupe._id}a`} type="text" className='bg-violet border border-gray-400 rounded-md p-1 focus:outline-none focus:border-blue-500'></input>
                        <button className={`rounded-lg bg-rouge text-noir text-sm font-bold px-4 py-2`} onClick={() => ajouterMembre(groupe._id)}>Ajouter un membre</button>
                    </div>
                </div>
            ))}
            <h3 className='text-xl text-noir font-bold'>Mes groupes :</h3>
            {groupsImMember.map((groupe, index) => (
                <div className="dropdown-container" key={index}>
                    <p onClick={() => toggleDropdownImMember(index)} className="toggle-text" style={{ fontWeight: 'bold' }}>
                        {groupe.nom}
                    </p>
                    <div className={`dropdown-content ${expandedItemsImMember[index] ? 'expanded' : ''}`}>
                        <p>Membres :</p>
                        <ul>
                            {groupe.nomsMembres.map((membre, i) => (
                                <li key={i}>{membre}</li>
                            ))}

                        </ul>
                    </div>
                </div>
            ))}
        </div>
    );
}